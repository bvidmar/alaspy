# -*- coding: utf-8 -*-
""" The LAS PointDataRecords module.

    :Author:
        - 2010-2012 Nicola Creati and Roberto Vidmar
        - 20180531 Roberto Vidmar Python3

    :Copyright: 2010-2018
                Nicola Creati <ncreati@inogs.it>
                Roberto Vidmar <rvidmar@inogs.it>

    :License: MIT/X11 License (see :download:`LICENSE.txt
                                <../../LICENSE.txt>`)
"""

import numexpr as ne
import numpy as np
import mmap

# Local imports
from . header import PointDataRecord
from . lasExceptions import (FileCreationError, InvalidPointDataFormatID,
                           MandatoryFieldMissing, UnimplementedException,
                           NoRecords)


# Functions:
#------------------------------------------------------------------------------
def sumLists(l1, l2):
    """ Return element by element sum of the two lists

        Args:
            l1 (list): fisrt list to add
            l2 (list): second list to add

        Returns:
           element by element list sum of l1 and l2

        .. warning:: the returned list will have the same number of elements
                     of the shortest one
    """
    return map(sum, zip(l1, l2))


#------------------------------------------------------------------------------
def checkForMandatoryFields(points):
    """ Check if points object has the right structure

        Args:
            points (:class:`dict`, :class:`numpy.ndarray` or\
                    :class:`numpy.memmap`): points instance

        Raises:
            :class:`~alaspy.lasExceptions.MandatoryFieldMissing`
    """
    if isinstance(points, dict):
        missingFields = set("XYZ").difference(set(points.keys()))

    else:
        missingFields = set("XYZ").difference(set(points[0].dtype.names))

    if missingFields:
        msg = ("Mandatory field(s) %s missing in points." % str(
                tuple(missingFields)))
        raise MandatoryFieldMissing(msg)


#------------------------------------------------------------------------------
def hasGPSTime(points):
    """ Return True if points object has GPSTime field

        Args:
            points (:class:`dict`, :class:`numpy.ndarray` or\
                    :class:`numpy.memmap`): points instance

        Returns:
            True if points object has GPSTime field else False
    """
    if isinstance(points, dict):
        missingFields = set(('GPStime', )).difference(set(points.keys()))
    else:
        missingFields = set(('GPSTime', )).difference(
                set(points[0].dtype.names))

    if missingFields:
        return False
    else:
        return True


#==============================================================================
class PointDataRecords(object):
    """ The Point Data Records object class
    """

    def __init__(self, parent):
        """ Create a new instance of the WHOLE dataset

            Args:
                parent (:class:`~alaspy.__init__.LAS`): LAS instance

            .. note:: No memory will be allocated yet
        """
        self._parent = parent
        self._header = parent.header()
        self._stopped = False  # is it necessary? Ask Nik.
        self._mapFile = None  # Memory mapped array
        self._removedRecords = None  # Number of records removed while reading

    def __repr__(self):
        cname = self.__class__.__name__
        s = "%s container opened for " % cname
        if self._parent.isReadMode():
            s += "reading:\n"
            s += ("Records can be returned in a Python container using the\n"
                  "%s().read() method or with usual indexing:\n"
                  "i.e. %s()[i]" % (cname, cname))
        else:
            s += "writing:\n"
            s += ("Records can be written to file using the\n"
                  "%s().write() method" % cname)
        return s

    def __getitem__(self, index):
        """ Return PointDataRecord at index

            Args:
                index (int): index of point to retrieve

            Returns:
                :class:`~alaspy.header.PointDataRecord` instance at index
        """
        if not self._header.isReadMode():
            raise IOError("%s not opened for reading" %
                          self._header.__class__.__name__)
        self._header.fid().seek(self._header.getOffsetToPointData() +
                                self._header.getPointDataReclen() * index)
        point = PointDataRecord(self._header, index)
        return point

    def _makeDtypes(self, fields, asFloat, withGPS):
        """ Return dtype both for input and output

            Args:
                fields (list, tuple or string): field names (strings) from
                            header pointDataRecordDtype() method

                             * or: the string 'all'
                             * or: the string 'short', shortcut for
                               (X(f4),Y(f4),Z(f4),Intensity, Classification)

                            In this case only (X, Y, Z, Intensity,
                            Classification) will be returned.
                            If offset parameter is not None
                            X, Y, Z values returned will be float32
                asFloat (bool): generate dtype float instead of double for\
                        X, Y, Z
                withGPS (bool): generate dtype with GPSTime field

            Returns:
                inputDtype, outputDtype tuple of :class:`numpy.dtype`
        """
        if fields in ('all', 'short'):
            inputDtype = self._header.pointDataRecordDtype('all')
        else:
            inputDtype = self._header.pointDataRecordDtype(fields)

        if fields == 'short':
            # Short output, we don't care about all fields...
            if asFloat:
                floatFormat = '<f4'
            else:
                floatFormat = '<f8'
            outputDtype = [('X', floatFormat), ('Y', floatFormat),
                           ('Z', floatFormat), ('Intensity', '<u2'),
                           ('Classification', '|u1')]
            if withGPS:
                outputDtype.append(('GPSTime', '<f8'))
            outputDtype = np.dtype(outputDtype)
        else:
            descr = inputDtype.descr
            for i, f in enumerate(descr):
                if f[0] in set("XYZ"):
                    descr[i] = (f[0], '<f8')
            outputDtype = np.dtype(descr)
        return inputDtype, outputDtype

    def _rescaleXYZ(self, data, offsets, asFloat):
        """ Rescale x, y, z fields in data to their original value,
            *subtracting* optionally offset tuple from x and y.

            Args:
                data (:class:`numpy.ndarray`): dataset to rescale
                offsets (tuple): Offsets tuple to apply to data.
                    These value will be *subtracted*
                    New x = x - offsets[0], New y = y - offsets[1]
                    (default is to apply offsets from Header)
                asFloat (bool): generate dtype float instead of double
                    for X, Y, Z

            Returns:
                xyz tuple of values
        """
        # Get scales from Header
        scales = self._header.scaleFactors()
        # Do not move zero
        zeros = (0., 0., 0.)
        if offsets is None:
            # Get offsets from Header
            offsets = self._header.offsets()
            if asFloat:
                zeros = (self._header.getXmin(), self._header.getYmin(), 0)

        xyz = tuple()
        for i, scale, offset, zero in zip("XYZ", scales, offsets, zeros):
            xyz += (ne.evaluate(
                    '((c * scale) + offset) - minimum',
                    local_dict=dict(
                            c=data[i], scale=scale, offset=offset,
                            minimum=zero)), )
        return xyz

    def _data2points(self, data, totalRecsRead, xyz, points):
        """ Move data to final destination

            Args:
                data (:class:`numpy.ndarray`): dataset to rescale
                totalRecsRead (int): total number of records read
                xyz (tuple): xyz values
                points (:class:`dict`, :class:`numpy.ndarray` or\
                        :class:`numpy.memmap`): points object

            Returns:
                points instance (same type of points argument)
        """

        #----------------------------------------------------------------------
        def getData(n):
            """ Return the array for field name `n` of the data object
                or a zero filled array of the same size if data has no field `n`
            """
            if n in data.dtype.names:
                return data[n]
            else:
                nDtype = [f[1] for f in points.dtype.descr if f[0] == n][0]
                return np.zeros(shape=data['X'].shape, dtype=nDtype)

        #----------------------------------------------------------------------
        recsValid = data.shape[0]
        xyzSet = set("XYZ")
        if isinstance(points, dict):
            if not points.has_key('X'):
                # dict is empty!
                for c, i in zip(xyz, "XYZ"):
                    points[i] = c.astype(points.dtype.fields[i][0].type)
                # Move remaining fields
                for n in set(points.dtype.names).difference(xyzSet):
                    if n == 'Classification':
                        points[n] = getData(n) & 0b11111
                    elif (n == 'GPSTime') and (
                            self._header.getVersionMinor() == 2):
                        if self._header.getGlobalEncoding() == 1:
                            points[n] = getData(n) - 1E9
                    else:
                        points[n] = getData(n)
            else:
                # dict initialized
                for c, i in zip(xyz, "XYZ"):
                    points[i] = np.append(
                            points[i],
                            c.astype(points.dtype.fields[i][0].type))

                # Move remaining fields
                for n in set(points.dtype.names).difference(xyzSet):
                    points[n] = np.append(points[n], getData(n))
        else:
            # No dict, ndarray or mmaped array
            for c, i in zip(xyz, "XYZ"):
                points[i][totalRecsRead:totalRecsRead + recsValid] = c
            # Copy remaining fields
            for n in set(points.dtype.names).difference(xyzSet):
                points[n][totalRecsRead:totalRecsRead + recsValid] = getData(n)

        return points

#------------------------------------------------------------------------------
# Public methods
#------------------------------------------------------------------------------

    def read(self,
             recStart=0,
             recEnd=-1,
             skip=None,
             recsPerChunk=None,
             offsets=None,
             fields='all',
             unique=False,
             withGPS=False,
             pointsDtype=dict,
             asFloat=False):
        """ Read all points and return a Python container according to
            `pointsDtype`

            Args:
                recStart (int): Record number to start from (default 0)
                recEnd (int): Last record to read,  default is -1, the number
                        of records from the header
                skip (int): Integer number of records to skip while reading
                recsPerChunk (int): Read file in recsPerChunk chunks
                offsets (tuple): Offsets tuple to apply to data.
                        These value will be *subtracted*
                        New x = x - offsets[0], New y = y - offsets[1]
                        (default is to apply offsets from Header)
                fields (tuple or string):
                        * List or tuple of field names (strings) from header
                          pointDataRecordDtype() method or:
                        * the string 'all' or:
                        * the string 'short', shortcut for
                          (X, Y, Z), Intensity, Classification)
                          In this case only (X, Y, Z, Intensity,
                          Classification) will be returned.
                          If offset parameter is not None
                          X, Y, Z values returned will be moved accordingly
                unique (bool): If True remove duplicate data points (only for
                        numpy objects)
                withGPS (bool): If True adds GPS data to short output
                pointsDtype (:class:`dict`, :class:`numpy.array` or\
                        :class:`str`): Points will be saved according to
                        argument type:

                            * :class:`dict` -> Python dictionary
                            * :class:`numpy.array` -> numpy ndarray
                            * :class:`str` -> numpy memory mapped array
                              file name

                asFloat (bool): if True output format of X, Y, Z fields will be
                        float (f4) instead of double (f8).
                        In this case an offset of None means that offset
                        will be computed from Header xmin, ymin, zmin

            Raises:
                :class:`~alaspy.lasExceptions.FileCreationError`, \
                    :class:`~alaspy.lasExceptions.UnimplementedException`, \
                    :class:`IOError`
        """
        if not self._header.isReadMode():
            raise IOError("%s not opened for reading" %
                          self._header.__class__.__name__)

        # If the point data format is not 1.1 or 1.3 GPS field
        # is missing from records
        if withGPS and (self._header.getPointDataFormatID() not in (1, 3)):
            withGPS = False
        # Create data types for reading and saving points
        inputDtype, outputDtype = self._makeDtypes(fields, asFloat, withGPS)

        numPointRecords = self._header.getNumPointRecords()
        if (recEnd == -1) and (recStart == 0):
            recsToRead = numPointRecords
        elif (recEnd == -1) and (recStart > 0):
            recsToRead = numPointRecords - recStart
        else:
            recsToRead = recEnd - recStart

        output_record_count = recsToRead if not skip else (
                1 + recsToRead / skip)

        # Allocate memory to hold all the points according to data type
        if (type(pointsDtype) is type(np.ndarray)
                    or isinstance(pointsDtype, np.ndarray)):
            # Create return empty ndarray
            points = np.empty(shape=(output_record_count, ), dtype=outputDtype)
        elif type(pointsDtype) is type(dict) or isinstance(pointsDtype, dict):
            # Create return empty dictionary
            points = {}
        else:
            if output_record_count < 1:
                raise NoRecords("LAS file is empty!")
            # Create an empty memory mapped ndarray
            try:
                points = np.memmap(
                        pointsDtype,
                        dtype=outputDtype,
                        mode='w+',
                        shape=output_record_count)
            except IOError as e:
                msg = ("Cannot create numpy memory mapped file: %s" % e)
                raise FileCreationError(msg)
            else:
                self._mapFile = pointsDtype

        # Compute number of record to read at each iteration
        if recsPerChunk:
            # We want to read chunkwise:
            recsPerChunk = min(recsToRead, int(recsPerChunk))
        else:
            # Read "Hole in one"
            recsPerChunk = recsToRead

        # Move file pointer to start of data
        recsize = self._header.pointDataRecordSize()
        offset = self._header.getOffsetToPointData() + recsize * recStart
        self._header.fid().seek(offset)

        info = ('Reading', self._parent.filename())
        self._parent.progress(0, info)

        # File reading loop, executed once in case nRecs is large enough
        remainingRecords = recsToRead
        numChunks = 0  # Number of data chunks read
        totalRecsRead = 0
        ### mmap experimental file support ###
        mapFile = mmap.mmap(
                self._header.fid().fileno(), 0, access=mmap.ACCESS_READ)
        data = np.arange(0)
        ###
        while remainingRecords and not self._stopped:
            data = np.frombuffer(
                    mapFile, inputDtype, offset=offset + data.nbytes)
            #data = np.fromfile(self._header.fid(), count=recsPerChunk,
            #  dtype=inputDtype)
            recsReadThisCunk = data.shape[0]
            numChunks += 1

            # Apply decimation if set
            data = data[::skip]
            # Rescale X, Y, Z
            xyz = self._rescaleXYZ(data, offsets, asFloat)
            # Move data chunk into points
            points = self._data2points(data, totalRecsRead, xyz, points)
            # Save number of records actually read
            totalRecsRead += recsReadThisCunk
            remainingRecords -= recsReadThisCunk

            # Adjust recsPerChunk
            recsPerChunk = min(recsPerChunk, remainingRecords)

            progress = 1. * totalRecsRead / numPointRecords
            self._parent.progress(progress, info)

        self._removedRecords = 0
        if unique:
            if type(pointsDtype) is type(dict) or isinstance(
                    pointsDtype, dict):
                raise UnimplementedException(
                        "Cannot remove duplicate records from dict objects.")
            else:
                # Remove duplicates
                points = np.unique(points)
                self._removedRecords = totalRecsRead - points['X'].shape[0]
        return points

    def removedRecords(self):
        """ Return the number of removed records while reading in case of
            duplicate records

            Returns:
                Number of removed records while reading (int) or None
        """
        return self._removedRecords

    def write(self,
              points,
              recStart=0,
              recEnd=-1,
              skip=None,
              recsPerChunk=None,
              offsets=None):
        """ Write LAS Header, Variable Length Records **AND** binary record(s)
            to file and leave file **CLOSED**

            Args:
                points (:class:`numpy.ndarray`, :class:`numpy.memmap`\
                        or :class:`dict`): either Recarray, Memory Mapped
                        Array or dict object containing LASER points or
                        list or tuple of the same objects.
                recStart (int): Record number to start from (default 0)
                recEnd (int): Last record to write,  (default is -1, the
                        number of records in the `points` instance)
                skip (int): Integer number of records to skip while writing.
                        Resample output so that the number of points written
                        is equal to (num of points) / skip (default None)
                recsPerChunk (int): Write file in recsPerChunk chunks
                        (default None)
                        (Useful with large datasets)
                offsets (tuple of float): offsets tuple to apply to x, y, z
                        data (default None)

            Raises:
                :class:`~alaspy.lasExceptions.InvalidPointDataFormatID`,
                        :class:`ValueError`

            .. warning:: Leaves file **CLOSED**
        """
        # Check for mandatory fields
        checkForMandatoryFields(points)

        if recEnd == -1:
            # End is last point of dataset
            recEnd = points['X'].size

        # Sanity check
        if recEnd < recStart:
            raise ValueError("start (%d) must be less than recend (%d)" %
                             (recStart, recEnd))

        # Compute number of output records
        recsToWrite = points['X'][recStart:recEnd:skip].shape[0]

        # Compute number of record to write at each iteration
        if recsPerChunk:
            # We want to write chunkwise:
            recsPerChunk = int(min(recsToWrite, recsPerChunk))
        else:
            # Write "Hole in one"
            recsPerChunk = recsToWrite

        if (hasGPSTime(points)
                and (self._header.getPointDataFormatID() in (0, 2))):
            raise InvalidPointDataFormatID(
                    "PointDataFormatID is %d but dataset *HAS* GPS Time info."
                    % self._header.getPointDataFormatID())

        # Create output structure
        lasDtype = self._header.pointDataRecordDtype()
        nonMandatoryFields = set(lasDtype.names).difference(set('XYZ'))
        output = np.zeros(shape=recsToWrite, dtype=lasDtype)

        min_xyz = [points[i].min() for i in "XYZ"]
        max_xyz = [points[i].max() for i in "XYZ"]

        # Set offsets in Header
        if offsets == None:
            offsets = min_xyz
        self._header.setOffsets(*offsets)

        scaleFactors = self._header.scaleFactors()
        for i, f in enumerate("XYZ"):
            output[f] = (np.round(
                    (points[f][recStart:recEnd:skip] - offsets[i]) /
                    scaleFactors[i])).flatten()

        # NON MANDATORY FIELDS:
        for f in nonMandatoryFields:
            if isinstance(points, dict):
                if points.has_key(f):
                    output[f] = points[f][recStart:recEnd:skip]
            else:
                if f in points.dtype.names:
                    output[f] = points[f][recStart:recEnd:skip]

        # Update Header
        self._header.setRanges(
                sumLists(min_xyz, offsets), sumLists(max_xyz, offsets))
        self._header.setNumPointRecords(output.shape[0])

        if self._header.getPointDataFormatID() < 6:
            byteData = output['AByte'][recStart:recEnd:skip]
        else:
            byteData = output['2Bytes'][recStart:recEnd:skip]
        # Count points by return number
        returnNumbers = PointDataRecord.getReturnNumber(byteData)
        numPoints = [
                (returnNumbers == i + 1).sum()
                for i, dummy in enumerate(self._header.getNumPointsByReturn())
        ]
        # Update header
        self._header.setNumPointsByReturn(numPoints)

        #######################################
        # RIEN NE VA PLUS! Write Header and VLR
        #######################################
        info = ('Writing', self._parent.filename())
        self._parent.progress(0, info)
        self._header.write()

        # File writing loop, executed once in case nRecs is large enough
        remainingRecords = recsToWrite

        recsWritten = 0
        while remainingRecords:
            # Adjust recsPerChunk
            recsPerChunk = min(recsPerChunk, remainingRecords)
            start = recsToWrite - remainingRecords
            end = start + recsPerChunk
            output[start:end].tofile(self._parent.fid())
            # Save number of records actually read
            recsWritten += recsPerChunk
            remainingRecords -= recsPerChunk
            # Memory mapped array?
            progress = 1. * recsWritten / recsToWrite
            self._parent.progress(progress, info)
        self._parent.fid().close()

    def mapFile(self):
        """ Return name of Memory Mapped file or None

            Returns:
                Name of Memory Mapped file or None
        """
        return self._mapFile
