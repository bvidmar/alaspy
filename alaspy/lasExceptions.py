# -*- coding: utf-8 -*-
""" :class:`alaspy` Exception Classes

    :Author:
      - 2009-2012 Roberto Vidmar
      - 20180531 Roberto Vidmar Python3

    :Copyright: 2009-2018
                Nicola Creati <ncreati@inogs.it>
                Roberto Vidmar <rvidmar@inogs.it>

    :License: MIT/X11 License (see :download:`LICENSE.txt
                               <../../LICENSE.txt>`)
"""


#==============================================================================
class LasException(Exception):
    """ Generic LAS module exception class
    """

    def __init__(self, *args):
        super(LasException, self).__init__(*args)
        self.args = args

    def __repr__(self):
        return repr(self.args[0])

    def __str__(self):
        return str(self.args[0])


#==============================================================================
class InvalidLASFile(LasException):
    """ Invalid LAS file exception class
    """

    def __init__(self, *args):
        super(InvalidLASFile, self).__init__(*args)


#==============================================================================
class InvalidPointDataFormatID(LasException):
    """ Invalid PointDataFormatID exception class
    """

    def __init__(self, *args):
        super(InvalidPointDataFormatID, self).__init__(*args)


#==============================================================================
class FileCreationError(LasException):
    """ File Creation Error exception class
    """

    def __init__(self, *args):
        super(FileCreationError, self).__init__(*args)


#==============================================================================
class MandatoryFieldMissing(LasException):
    """ Mandatory Field missing exception class
    """

    def __init__(self, *args):
        super(MandatoryFieldMissing, self).__init__(*args)


#==============================================================================
class MustBeImplemented(LasException):
    """ Must Be Implemented exception class
    """

    def __init__(self, *args):
        super(MustBeImplemented, self).__init__(*args)


#==============================================================================
class UnimplementedException(LasException):
    """ Unimplemented exception class
    """

    def __init__(self, *args):
        super(UnimplementedException, self).__init__(*args)


#==============================================================================
class ConversionFailed(LasException):
    """ Conversion Failed exception class
    """

    def __init__(self, *args):
        super(ConversionFailed, self).__init__(*args)


#==============================================================================
class NoRecords(LasException):
    """ No Records exception class
    """

    def __init__(self, *args):
        super(NoRecords, self).__init__(*args)
