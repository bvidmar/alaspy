""" alaspy module to read and write LAS files.

  :Author:
    - 2010-2012 Nicola Creati and Roberto Vidmar
    - 20180531 Roberto Vidmar Python3

    :Copyright: 2010-2018
              Nicola Creati <ncreati@inogs.it>
              Roberto Vidmar <rvidmar@inogs.it>

    :License: MIT/X11 License (see :download:`LICENSE.txt
                             <../../LICENSE.txt>`)
"""
#==============================================================================
import os

# Local libraries
from . header import Header, getLongest
from . pointDataRecords import PointDataRecords
from . lasExceptions import LasException


#==============================================================================
class LAS(object):
    """ The LAS file class
    """
    # Keep trace of opened files
    _lock = []

    def __init__(self, filename, mode='r', *args, **kargs):
        """ Create a new :class:`LAS` instance.

            Args:
                filename (:class:`str`): LAS file to open
                mode (:class:`str`): file mode ('r' or 'w')

            Keyword Args:
                Optional kargs will be passed to Header
        """
        self._filename = os.path.abspath(filename)

        # Check if file is already opened
        for f in self._lock:
            if f == self._filename:
                raise LasException(
                        "File %s is already in use. Close the file or delete "
                        "the reference to it" % self._filename)
        self._lock.append(self._filename)

        # Now open the file
        if mode == 'w':
            # Write mode:  Binary mode must be explicitly set on Window
            self._fid = open(self._filename, 'wb')
        else:
            # Read mode: Binary mode must be explicitly set on Window
            self._fid = open(self._filename, 'rb')

        self._header = Header(self, *args, **kargs)
        self._PointDataRecords = PointDataRecords(self)

    def __repr__(self):
        """ Return string representation of the :class:`LAS`

            Returns:
                String representation of the :class:`LAS`
        """
        title = '%s:\n  Filename: %s\n' % (self.__class__.__name__,
                                           self._filename)
        headerRepr = repr(self._header) + '\n'
        vlrRepr = "\n".join(
                [repr(vlr) for vlr in self._header.varLengthRecords()])
        lenmax = max(getLongest(headerRepr), getLongest(vlrRepr))
        title += '-' * (lenmax + 2) + '\n'
        return title + headerRepr + vlrRepr

#------------------------------------------------------------------------------
# Public methods
#------------------------------------------------------------------------------

    def fid(self):
        """ Return LAS file identifier

            Returns:
                LAS file identifier (:class:`file`)
        """
        return self._fid

    def close(self):
        """ Close las file and remove lock
        """
        self._lock.remove(self._filename)
        self._fid.close()

    def filename(self):
        """ Return Las filename

            Returns:
                Las filename (:class:`str`)
        """
        return self._filename

    def height(self):
        """ Return Las dataset y range

            Returns:
                Las dataset y range (:class:`float`)
        """
        y0, y1 = self._header.yRange()
        return y1 - y0

    def width(self):
        """ Return dataset x range

            Returns:
                Las dataset x range (:class:`float`)
        """
        x0, x1 = self._header.xRange()
        return x1 - x0

    def header(self):
        """ Return Las Header

            Returns:
                Las Header (:class:`~alaspy.header.Header`): instance
        """
        return self._header

    def isReadMode(self):
        """ Return True if Las file is in 'read' mode

            Returns:
                True if Las file is in 'read' mode else False (:class:`bool`)
        """
        return self._fid.mode.startswith('r')

    def records(self):
        """ Return Point Data Records instance

            Returns:
                Point Data Records
                (:class:`~alaspy.pointDataRecords.PointDataRecords`): instance
        """
        return self._PointDataRecords

    def progress(self, value, info=None):
        """ Must be reimplemented in derived class to get the overal point
            reading progress

            Args:
                value (:class:`float`): progress value in [0, 1]
                info (:class:`any`): info instance
        """
        if info is not None:
            print('%s: Progress %s' % (str(info), value * 100.))
        else:
            print('Progress %s' % (value * 100.))

    def write(self, points, **kargs):
        self._PointDataRecords.write(points, **kargs)


#==============================================================================

# I love Python!
#LAS.write.__func__.__doc__ = PointDataRecords.write.__doc__
LAS.write.__doc__ = PointDataRecords.write.__doc__


#==============================================================================
if __name__ == '__main__':
    import sys
    import numpy as np
    from . header import VLRecord
    trueExcepthook = sys.excepthook

    def excepthook(t, value, traceback):
        if t.__base__ in (LasException, IOError):
            print("Unhandled Exception <%s>: %s" % (t.__name__, value))
        else:
            trueExcepthook(type, value, traceback)

    sys.excepthook = excepthook

    las = LAS(sys.argv[1])
    print("The header repr:\n", las.header())
    print("The WKT:")
    print(las.header().prettyWkt())
    #data = las.records().read(pointsDtype=np.array(()), fields='short',
            #unique=True, asFloat=False, recsPerChunk=3)
    data = las.records().read(
            pointsDtype='/tmp/ccqq',
            fields='short',
            unique=True,
            asFloat=False,
            recsPerChunk=3)
    #data = las.records().read(pointsDtype=dict(), recsPerChunk=3)
    #data = las.records().read(pointsDtype=dict, recsPerChunk=3, fields='short',
            #asFloat=True)
    #data = las.records().read(pointsDtype=dict, fields='short', asFloat=True)
    print("The LAS file repr:\n", las)

    if os.path.basename(las.filename()) == 'test.las':
        from IPython import embed; embed()
        sys.exit()
    # Create a new LAS file
    newlas = LAS(
            'test.las',
            'w',
            version=las.header().version(),
            pointDataFormatID=las.header().getPointDataFormatID())
    # Set WKT to new file from old one
    newHeader = newlas.header()
    newHeader.setWkt(las.header().wkt())
    # Create new 'OGS' VLRecord
    payload = dict(d1="Some secret data", d2="More secret...")
    ogs = VLRecord(
            newHeader,
            VLRecord.OGS,
            recordID=1,
            description="A private VLR",
            data=payload)
    # Add new VLR to header
    newHeader.addVarLengthRecord(ogs)
    # Write new LAS file to disk
    newlas.write(data, offsets=las.header().offsets())
    print("NEW LAS file 'test.las' written:", newlas)
