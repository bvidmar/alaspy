.. _CARS: http://www.inogs.it/en/content/cartography-and-remote-sensing 

.. _GDAL:  http://www.gdal.org

.. _numpy:  http://numpy.scipy.org

alaspy Python Package Documentation
===================================

Foreword
********
alaspy has been developed by two programmers working in the CARS_ 
(CArtography and Remote Sensing) group of the IRI Research Section at
*OGS - Istituto Nazionale di Oceanografia e di
Geofisica Sperimentale*.

Python is their favourite programming language since 2006.

The authors:

**Nicola Creati, Roberto Vidmar**

.. image:: images/creati.jpg
   :height: 134 px
   :width:  100 px 
.. image:: images/vidmar.jpg
   :height: 134 px
   :width:  100 px 

What is alaspy?
***************

alaspy is **ANOTHER** Python package to handle LAS files.
It supports reading and
writing LAS files from Version 1.0 to Version 1.3.
(Version 1.4 is not yet fully implemented).
The :class:`~alaspy.las.LasObject` class has methods to access the

  * :class:`~alaspy.header.Header` instance and all of the 
    
    * :class:`~alaspy.header.VLRecord` instances that contain geocoding
      information as well as user defined information

    .. note:: This class has also methods to convert geocoding information
       to (:class:`~alaspy.header.Header.wkt`) and from WKT
       (:class:`~alaspy.header.Header.setWkt`) as well as Proj.4
       (:class:`~alaspy.header.Header.proj4`,
       :class:`~alaspy.header.Header.setProj4`) if the `osgeo` module (from
       the GDAL_ suite) is available. 

  * :class:`~alaspy.pointDataRecords.PointDataRecords` instance that contain
    all LASER :class:`~alaspy.header.PointDataRecord` returns
    according to Point Data Record Format as well as a Python `dict` (or
    numpy_ array container) with the whole dataset for fast access and
    processing.

It relies on numpy_ for speed and GDAL_ for geocoding information.

The package has ben developed with Python 3.6.5, numpy 1.14.3, GDAL 2020300


alaspy Package Reference
************************

.. toctree::
  :maxdepth: 3

   alaspy Package modules: <alaspy>

.. warning::
   This code has been tested *only* on Linux (Ubuntu 18.04) but should work
   also on Mac and Windows (Xp and newer versions).

.. warning::
   This is work in progress!

Indices and Tables
******************

* :ref:`genindex`
* :ref:`modindex`
