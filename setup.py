from setuptools import setup
# setuptools allows "python setup.py develop"

setup(name='alaspy',
    version='0.2',
    description='Another packege to handle LAS files',
    author='Roberto Vidmar',
    url='http://www.inogs.it/it/users/roberto-vidmar',
    packages=['alaspy'],
    package_data={
        #'ccqw': ['icons_rc.py', ],
    }
 )
