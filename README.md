# ALASpy is **ANOTHER** Python package to handle LAS files

It supports reading and writing LAS files from Version 1.0 to Version 1.3.
(Version 1.4 is not yet fully implemented).

The LasObject class has methods to access the Header instance and all of
the VLRecord instances that contain geocoding information as well as user
defined information.

This class has also methods to convert geocoding information to (wkt)
and from WKT (setWkt) as well as Proj.4 (proj4, setProj4) if the osgeo
module (from the GDAL suite) is available.

A Points instance contain all LASER PointDataRecord returns
(according to Point Data Record Format) in a memory mapped file for fast
access and processing.


```
#!

sudo apt-get install python-dev
wget http://pyproj.googlecode.com/files/pyproj-1.9.0.tar.gz
tar zxvf pyproj-1.9.0.tar.gz
cd pyproj-1.9.0
sudo python setup.py install
```
--------------------------------------
## Documentation ##

Documentation about alalpy can be found
[here](https://bitbucket.org/bvidmar/alaspy/src/master/doc/html/index.html).

## Get the code ##

To test the package check out a read-only working copy anonymously over *https*:


```
#!

git clone https://bitbucket.org/bvidmar/alaspy
```
then


```
#!

cd alaspy
python setup.py install
```

### Who do I talk to? ###

* [Roberto Vidmar](http://www.inogs.it//users/roberto-vidmar)
* [Nicola Creati](http://www.inogs.it//users/nicola-creati)
